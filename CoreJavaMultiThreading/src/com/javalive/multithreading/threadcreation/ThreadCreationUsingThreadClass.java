package com.javalive.multithreading.threadcreation;

/*
 * This is another way to create a thread by a new class that extends Thread class 
 * and create an instance of that class. The extending class must override run() 
 * method which is the entry point of new thread.
 * In this case also, we must override the run() and then use the start() method to run the thread.
 * 
 * What if we call run() method directly without using start() method?
 * Doing so, the thread won't be allocated a new call stack, and it will start running in the current 
 * call stack, that is the call stack of the main thread. Hence Multithreading won't be there.
 * 
 * Can we Start a thread twice?
 * No, a thread cannot be started twice. If you try to do so, IllegalThreadStateException 
 * will be thrown.
*/

class MultithreadingDemo1 extends Thread {
	public void run() {
		try {
			// Displaying the thread that is running
			System.out.println("Thread " + Thread.currentThread().getId() + " is running");

		} catch (Exception e) {
			// Throwing an exception
			System.out.println("Exception is caught");
		}
	}
}

public class ThreadCreationUsingThreadClass {
	public static void main(String[] args) {
		int n = 8; // Number of threads
		for (int i = 0; i < 8; i++) {
			MultithreadingDemo1 object = new MultithreadingDemo1();
			object.start();
		}
	}
}
