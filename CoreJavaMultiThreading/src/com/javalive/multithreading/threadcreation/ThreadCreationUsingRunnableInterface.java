package com.javalive.multithreading.threadcreation;


/*
 *The easiest way to create a thread is to create a class that implements the runnable interface. 
 *After implementing runnable interface, the class needs to implement the run() method.

 * Run Method Syntax:
 * public void run()
 * It introduces a concurrent thread into your program. This thread will end when run() method 
 * terminates.
 * You must specify the code that your thread will execute inside run() method.
 * run() method can call other methods, can use other classes and declare variables just like 
 * any other normal method. 
 * 
 * To call the run()method, start() method is used. On calling start(), a new stack is provided to 
 * the thread and run() method is called to introduce the new thread into the program.
 * Note: If you are implementing Runnable interface in your class, then you need to explicitly create 
 * a Thread class object and need to pass the Runnable interface implemented class object as a 
 * parameter in its constructor.
 * 
*/
class MultithreadingDemo implements Runnable {
	public void run() {
		try {
			// Displaying the thread that is running
			System.out.println("Thread " + Thread.currentThread().getId() + " is running");

		} catch (Exception e) {
			// Throwing an exception
			System.out.println("Exception is caught");
		}
	}
}

public class ThreadCreationUsingRunnableInterface {
	public static void main(String[] args) {
		int n = 8; // Number of threads
		for (int i = 0; i < n; i++) {
			Thread object = new Thread(new MultithreadingDemo());
			object.start();
		}
	}
}
