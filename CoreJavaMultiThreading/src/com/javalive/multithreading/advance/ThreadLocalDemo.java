package com.javalive.multithreading.advance;

/*
 * The ThreadLocal class is used to create thread local variables which can only be read and 
 * written by the same thread. For example, if two threads are accessing code having reference
 * to same threadLocal variable then each thread will not see any modification to threadLocal 
 * variable done by other thread.
 * ThreadLocal Methods
 * Following is the list of important methods available in the ThreadLocal class.
 * Sr.No.	Method & Description
 * 1 public T get()
                   Returns the value in the current thread's copy of this thread-local variable.
 * 2 protected T initialValue()
                   Returns the current thread's "initial value" for this thread-local variable.
 * 3 public void remove()
                   Removes the current thread's value for this thread-local variable.
 * 4 public void set(T value)
                   Sets the current thread's copy of this thread-local variable to the specified value.
*/

class MyRunnable implements Runnable {
	private ThreadLocal<Integer> threadLocal = new ThreadLocal<Integer>();

	public void run() {
		threadLocal.set((int) (Math.random() * 100D));
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
		}
		System.out.println(threadLocal.get());
	}
}

public class ThreadLocalDemo {
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		MyRunnable runnableInstance = new MyRunnable();
		Thread t1 = new Thread(runnableInstance);
		Thread t6 = new Thread(runnableInstance);
		Thread t2 = new Thread(runnableInstance);
		Thread t7 = new Thread(runnableInstance);
		Thread t3 = new Thread(runnableInstance);
		Thread t8 = new Thread(runnableInstance);
		Thread t4 = new Thread(runnableInstance);
		Thread t9 = new Thread(runnableInstance);
		Thread t5 = new Thread(runnableInstance);
		Thread t10 = new Thread(runnableInstance);
		// this will call run() method
		t1.start();
		t6.start();
		t2.start();
		t7.start();
		t3.start();
		t8.start();
		t4.start();
		t9.start();
		t5.start();
		t10.start();
	}

}
