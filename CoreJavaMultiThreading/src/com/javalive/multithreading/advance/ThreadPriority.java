package com.javalive.multithreading.advance;

/**
 * @author www.itaspirants.com
 * @description In a Multi threading environment, thread scheduler assigns
 *              processor to a thread based on priority of thread. Whenever we
 *              create a thread in Java, it always has some priority assigned to
 *              it. Priority can either be given by JVM while creating the
 *              thread or it can be given by programmer explicitly. Accepted
 *              value of priority for a thread is in range of 1 to 10. There are
 *              3 static variables defined in Thread class for priority.
 * 
 *              public static int MIN_PRIORITY: This is minimum priority that a
 *              thread can have. Value for this is 1. public static int
 *              NORM_PRIORITY: This is default priority of a thread if do not
 *              explicitly define it. Value for this is 5. public static int
 *              MAX_PRIORITY: This is maximum priority of a thread. Value for
 *              this is 10.
 * 
 *              Get and Set Thread Priority: public final int getPriority():
 *              java.lang.Thread.getPriority() method returns priority of given
 *              thread. public final void setPriority(int newPriority):
 *              java.lang.Thread.setPriority() method changes the priority of
 *              thread to the value newPriority. This method throws
 *              IllegalArgumentException if value of parameter newPriority goes
 *              beyond minimum(1) and maximum(10) limit.
 * 
 *              Note:
 * 
 *              Thread with highest priority will get execution chance prior to
 *              other threads. Suppose there are 3 threads t1, t2 and t3 with
 *              priorities 4, 6 and 1. So, thread t2 will execute first based on
 *              maximum priority 6 after that t1 will execute and then t3.
 *              Default priority for main thread is always 5, it can be changed
 *              later. Default priority for all other threads depends on the
 *              priority of parent thread.
 * 
 *              Example of getPriority() and setPriority()
 * 
 * 
 */
public class ThreadPriority extends Thread {
	public void run() {
		System.out.println(Thread.currentThread()); // This method is static.
	}

	public static void main(String[] args) {
		ThreadPriority a = new ThreadPriority();
		Thread t1 = new Thread(a, "First Thread");
		Thread t2 = new Thread(a, "Second Thread");
		Thread t3 = new Thread(a, "Third Thread");

		t1.setPriority(4); // Setting the priority of first thread.
		t2.setPriority(2); // Setting the priority of second thread.
		t3.setPriority(8); // Setting the priority of third thread.

		t1.start();
		t2.start();
		t3.start();
	}
}
