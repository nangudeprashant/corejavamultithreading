package com.javalive.multithreading.synchronization;

/*
 * A Synchronized block is a piece of code that can be used to perform synchronization on any 
 * specific resource of the method. A Synchronized block is used to lock an object for any 
 * shared resource and the scope of a synchronized block is smaller than the synchronized method. 
 * Syntax
    synchronized(object) {
   // block of code
   }
Here, an object is a reference to the object being synchronized. A synchronized block ensures that 
a call to a method that is a member of an object occurs only after the current thread has successfully
entered the object�s monitor.
*/
class Table {

	void printTable(int n) {
		synchronized (this) {// synchronized block
			for (int i = 1; i <= 10; i++) {
				System.out.println(n * i);
				try {
					Thread.sleep(400);
				} catch (Exception e) {
					System.out.println(e);
				}
			}
		}
	}// end of the method
}

class MyThread1 extends Thread {
	Table t;

	MyThread1(Table t) {
		this.t = t;
	}

	public void run() {
		t.printTable(5);
	}

}

class MyThread2 extends Thread {
	Table t;

	MyThread2(Table t) {
		this.t = t;
	}

	public void run() {
		t.printTable(100);
	}
}

public class BlockSynchronizationDemo {
	public static void main(String args[]) {
		Table obj = new Table();// only one object
		MyThread1 t1 = new MyThread1(obj);
		MyThread2 t2 = new MyThread2(obj);
		t1.start();
		t2.start();
	}
}
