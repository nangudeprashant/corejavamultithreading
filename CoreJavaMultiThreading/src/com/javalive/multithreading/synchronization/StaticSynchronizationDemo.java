package com.javalive.multithreading.synchronization;


/*
Static Synchronization
The Synchronized method may lose its behavior of getting an ordered output. When there are more
objects of a class, It acquires only the lock of the particular instance. To maintain the 
Synchronized behavior, we need a class-level lock rather than an instance-level lock which can be
achieved by Static Synchronization.
Static Synchronized method is also a method of synchronizing a method in java such that no two 
threads can act simultaneously static upon the synchronized method. The only difference is by 
using Static Synchronized. We are attaining a class-level lock such that only one thread will 
operate on the method. The Thread will acquire a class level lock of a java class, such that only 
one thread can act on the static synchronized method.

Syntax:
synchronized static return type class name{}
Note: When a class has both synchronized and static synchronized methods they can run parallelly ,
as those two methods require different locks.
*/
class TableDemo {

	synchronized static void printTable(int n) {
		for (int i = 1; i <= 10; i++) {
			System.out.println(n * i);
			try {
				Thread.sleep(400);
			} catch (Exception e) {
			}
		}
	}
}

class MyThread3 extends Thread {
	public void run() {
		TableDemo.printTable(1);
	}
}
/*
class MyThread4 extends Thread {
	public void run() {
		TableDemo.printTable(10);
	}
}

class MyThread5 extends Thread {
	public void run() {
		TableDemo.printTable(100);
	}
}

class MyThread6 extends Thread {
	public void run() {
		TableDemo.printTable(1000);
	}
}
*/
public class StaticSynchronizationDemo {
	public static void main(String t[]) {
		MyThread3 t1 = new MyThread3();
		MyThread3 t5 = new MyThread3();
		/*MyThread4 t2 = new MyThread4();
		MyThread5 t3 = new MyThread5();
		MyThread6 t4 = new MyThread6();*/
		t1.start();t5.start();
		/*t2.start();
		t3.start();
		t4.start();*/
	}

}
